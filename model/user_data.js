var mongoose = require('mongoose');

var Schema = mongoose.Schema;

    var dbforuser = new Schema({
        compress:String,
        battletag : String,
        rank : String,
        mmr : String,
        
        date: { type: Date, default: Date.now  }
});

module.exports = mongoose.model('dbforuser',dbforuser);
