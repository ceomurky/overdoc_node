const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const rp = require('request-promise');
const cheerio = require('cheerio');

const urlencode = require('urlencode');

const pako = require('pako');

const async = require('async');

var CORS = require('cors')();
 

app.use(CORS);


// var mysql = require('mysql');
// var connetion = mysql.createConnection({
//     host : 'localhost',
//     user : 'root',
//     password : '111111',
//     port : 3306,
//     database : 'overdoc'
// });


// connetion.connect();



var datasort = {
    "quickplay" : "#quickplay",
    "competitive" : "#competitive"
}

var datatree =
    {
        'ALL HEROES': '0x02E00000FFFFFFFF',
        'Reaper': '0x02E0000000000002',
        'Tracer': '0x02E0000000000003',
        'Mercy': '0x02E0000000000004',
        'Hanzo': '0x02E0000000000005',
        'Torbjörn': '0x02E0000000000006',
        'Reinhardt': '0x02E0000000000007',
        'Pharah': '0x02E0000000000008',
        'Winston': '0x02E0000000000009',
        'Widowmaker': '0x02E000000000000A',
        'Bastion': '0x02E0000000000015',
        'Symmetra': '0x02E0000000000016',
        'Zenyatta': '0x02E0000000000020',
        'Genji': '0x02E0000000000029',
        'Roadhog': '0x02E0000000000040',
        'McCree': '0x02E0000000000042',
        'Junkrat': '0x02E0000000000065',
        'Zarya': '0x02E0000000000068',
        'Soldier: 76': '0x02E000000000006E',
        'Lúcio': '0x02E0000000000079',
        'D.Va': '0x02E000000000007A',
        'Mei': '0x02E00000000000DD',
        'Sombra': '0x02E000000000012E',
        'Ana': '0x02E000000000013B',
        'Orisa': '0x02E000000000013E'
    };


app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


if (module === require.main) {
    const server = app.listen(process.env.PORT || 8080, () => {
    const port = server.address().port;
    console.log(`App listening on port ${port}`);
  });
}

app.get('/timer',function(req,res){
  res.json({time:new Date().getTime()});
});



app.get('/userData',function(req,res){
    //데이터가 db에 없으면 인자값으로 넘겨줌.
    if(req.query.region && req.query.battletag){
        getData_func(req.query.region,req.query.battletag,(getdb)=>{
            res.json(getdb);
        });
    }else{
        res.json({
            err : true, note : "invailed region or battletag"
        })
    }


});

app.get('/test',function(req,res){
    cargo.push({name:'test'},function (err) {
        getData_func('kr',"하늘보리-3724",(result)=>{
            res.send(result);   
        })
    });
})


function getData_func(region,betag,callback){
  
  var url=`https://playoverwatch.com/en-us/career/pc/${region}/${urlencode(betag)}`;
  var heroes = {
      user:{},
      data:{}
  };

  rp(url).then(function (htmlString) {
    var $ = cheerio.load(htmlString);
    for(var sort in datasort){
      heroes.data [sort] = {};
      heroes.data [sort]['ALL HEROES'] = {};
      $(`${datasort[sort]} div[class="row column"] div[data-category-id="overwatch.guid.0x0860000000000021"] .progress-2`).each(function(){
            if($(this).find(".description").text().match('hour')){
                var name = $(this).find(".title").text();
                heroes.data [sort][name] = {};
            }
      });
    }

    heroes.user['battletag'] = betag;
    var rank = $('div[class="bg-color-blue-dark u-invert"] div[class="competitive-rank"] div[class="u-align-center h6"]').html();
    var rankimg = $('div[class="bg-color-blue-dark u-invert"] div[class="competitive-rank"] img').attr("src");
    if(rank) 
    {
        heroes.user['rank'] = rank;
        heroes.user['rankimg'] = rankimg;
    }
    heroes.user['now'] = new Date().getTime();

    for(var sort in datasort){
        for(var tree in heroes.data [sort]){
        var code = datatree[tree];
        heroes.data [sort][tree] = {};
        $(`${datasort[sort]} div[data-category-id=${code}] div[class="column xs-12 md-6 xl-4"]`)
        .each(function () {
            var spanfider = $(this).find('span').text();


            //이렇게 세개의 데이터만 필요하기 때문에..
            heroes.data [sort][tree][spanfider]= {};
            $(this).find('tr').each(function(){
                if($(this).find('td').text()){
                var name;
                var result;
                var count = true;
                    $(this).find('td').each(function(){
                        if(count==true)
                        {
                            name = $(this).text();
                            count ++;
                        }
                        else
                        {
                            result = $(this).text();
                            count = false;
                        }
                    })
                    heroes.data [sort][tree][spanfider][name] = result;
                }
            }) // this is tr each
            // heros[sort][tree]['Combat']['killper'] =  heros[sort][tree]['Combat'] ? (heros[sort][tree]['Combat']['Eliminations'] / heros[sort][tree]['Deaths']['Deaths']) : 'null';
        })//$ each part
    }
  }

  //여기에서 생성된 heores를 docsmmr로 넘겨줌. -> return val은 compress에서 맹글어짐.
   callback({err:false,data:{heroes : heroes.data,user : heroes.user}});
//   DocsMmrFunc(heroes,(heroes)=>{
   
//   });
}).catch((err)=>{
    callback({err:true,log:'NotFound'});
})//rp
  
}//<getData-Func


//일단 폐지;;ㅋ
function DocsMmrFunc(heroes,callback){
    heroes ['competitive']['ALL HEROES']['Game']['Win Percentage'] = ((heroes ['competitive']['ALL HEROES']['Game']['Games Won'] / heroes ['competitive']['ALL HEROES']['Game']['Games Played']) * 100).toFixed() + '%';
    //개인별 승률은 제공해주는데 일단 승률은 제공안해줘서 직접만들고
    for(var sort in datasort){
        heroes [sort]['ALL HEROES']['Combat']['Eliminations per Life'] = (heroes [sort]['ALL HEROES']['Combat']['Eliminations'].replace(/,/g, '') / heroes [sort]['ALL HEROES']['Deaths']['Deaths'].replace(/,/g, '')).toFixed(3);
    } //이것역시 라이프당 킬수 가 나오지않아서 만들어줌


    callback(heroes);
}

